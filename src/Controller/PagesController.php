<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Default Controller.
 */
class PagesController extends AbstractController
{
    #[Route('/', name: 'frontpage')]
    public function index(): Response
    {
        $environment = ($_ENV['APP_ENV'] !== 'prod') ? " ({$_ENV['APP_ENV']})" : "";
        return $this->render('app.html.twig', [
            'site_name' => "my.cornerstone{$environment}",
            'title' => 'Welcome',
            'content' => null,
        ]);
    }
}
