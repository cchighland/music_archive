<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220425035526 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create user table.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (
            id INT AUTO_INCREMENT NOT NULL,
            username VARCHAR(180) NOT NULL,
            roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\',
            password VARCHAR(255) NOT NULL,
            fullname VARCHAR(255) DEFAULT NULL,
            email VARCHAR(255) DEFAULT NULL,
            UNIQUE INDEX UNIQ_8D93D649F85E0677 (username),
            PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');

        // Add initial admin user.
        $password = $this->hashPassword($_ENV['ADMIN_PASSWORD'] ?? 'password');
        $email = $_ENV['ADMIN_EMAIL'] ?? 'admin@example.com';
        $this->addSql("
            INSERT INTO `user`
            VALUES (1, 'admin', '[\"ROLE_USER\", \"ROLE_ADMIN\"]', '$password', 'Administrator', '$email')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
    }

    private function hashPassword(string $password): string
    {
        $factory = new PasswordHasherFactory([
            'common' => ['algorithm' => 'auto'],
        ]);
        return $factory->getPasswordHasher('common')->hash($password);
    }
}
