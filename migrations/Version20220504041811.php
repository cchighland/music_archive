<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504041811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add archive_directory table.';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE archive_directory (
            id INT AUTO_INCREMENT NOT NULL,
            name VARCHAR(190) NOT NULL,
            filepath VARCHAR(190) NOT NULL,
            created DATETIME NOT NULL,
            changed DATETIME NOT NULL,
            INDEX name_idx (name),
            UNIQUE INDEX path_idx (filepath),
            PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE archive_directory');
    }
}
